#!/bin/bash

printf "\nPress 1 to build project and run\nPress 2 to project + docker build and run\nPress 3 to run existing docker\nPress 4 to exit\n\n"

while true; do
    read -p "Select an option: " yn
    case $yn in
        [1]* ) 
			#[Press 1 to build and run]
			
			#MAVEN BUILD
			cp ./db/sample.db.bak ./db/sample.db
			./mvnw package && java -jar target/spring-boot-docker-0.0.1.war
						
			break;;
        [2]* ) 
        
			#[Press 2 to build and docker run]
        
			#MAVEN BUILD
			./mvnw package 
			mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.war)

			#DOCKER BUILD
			docker build -t springio/spring-boot-docker .

			# SERVER START
			docker run -p 8080:8080 springio/spring-boot-docker
        
			break;;
		[3]* )
		
			# SERVER START
			docker run -p 8080:8080 springio/spring-boot-docker
		
			break;;
        [4]* ) 
        
			exit;;
        * ) echo "Please select an option: ";;
    esac
done


