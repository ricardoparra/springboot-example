# Spring Boot Example

###### To execute:

    1º - chmod +x initScript.sh
    2º - bash initScript.sh

###### The program will start and you can enter one of the following options:

    Press 1 to build project and run
    Press 2 to project + docker build and run
    Press 3 to run existing docker
    Press 4 to exit

###### Use option 2 to build project, create and run a docker
###### Use option 3 to run an already created docker image
###### After go to your browser and access the page: http://127.0.0.1:8080/

![](./images/image.png)
