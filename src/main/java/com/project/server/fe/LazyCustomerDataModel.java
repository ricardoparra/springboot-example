package com.project.server.fe;

import java.util.List;
import java.util.Map;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

import com.project.sqlite.Customer;
import com.project.sqlite.CustomerService;

public class LazyCustomerDataModel extends LazyDataModel<Customer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4352116702071023604L;
	

	private CustomerService service;

	public LazyCustomerDataModel(CustomerService service) {
		this.service = service;
	}

	@Override
	public Customer getRowData(String rowKey) {
		return Customer.getRowData(rowKey);
	}

	@Override
	public String getRowKey(Customer customer) {
		return Customer.getRowKey(customer);
	}

	@Override
	public List<Customer> load(int first, int pageSize, Map<String, SortMeta> sortBy,
			Map<String, FilterMeta> filterBy) {

		Customer filter = null;

		if (!filterBy.isEmpty()) {
			filter = new Customer();
			for (Map.Entry<String, FilterMeta> entry : filterBy.entrySet()) {
				if (entry.getValue().getField().equals("id")) {
					filter.setId(Integer.parseInt(entry.getValue().getFilterValue().toString()));
				}
				if (entry.getValue().getField().equals("name")) {
					filter.setName(entry.getValue().getFilterValue().toString());
				}
				if (entry.getValue().getField().equals("phone")) {
					filter.setPhone(entry.getValue().getFilterValue().toString());
				}
				if (entry.getValue().getField().equals("country")) {
					filter.setCountry(entry.getValue().getFilterValue().toString());
				}
				if (entry.getValue().getField().equals("state")) {
					filter.setState(Integer.parseInt(entry.getValue().getFilterValue().toString()));
				}
			}
		}

		// SORT
		String sortFieldString = null;
		String sortOrderString = null;

		if (!sortBy.isEmpty()) {
			Map.Entry<String, SortMeta> entry = sortBy.entrySet().iterator().next();
			if (entry != null) {
				sortFieldString = entry.getValue().getField();
				SortOrder sortOrder = entry.getValue().getOrder();
				if (sortOrder != null) {
					int s = sortOrder.intValue();
					if (s == 0) {
						sortOrderString = "ASC";
					}
					if (s == 1) {
						sortOrderString = "DESC";
					}
				}
			}

		}

		List<Customer> customers = service.getCustomersByFilter(filter, first, pageSize, sortFieldString,
				sortOrderString);

		// rowCount
		setRowCount((int) service.getCountCustomersByFilter(filter));

		return customers;
	}

}