package com.project.sqlite;
 
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

 
@Entity
@Table(name = "customer")
public class Customer implements Serializable {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 7481345050257678978L;

	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;
 
    @Column(length = 50)
    public String name;
 
    @Column(length = 50)
    public String phone;
    
    @Column
    public Integer state;
    
    @Column
    public String country;
    
    
    public Customer() {
    	super();
    	
    }
    
    public Customer(Integer id, String name, String phone, Integer state, String country) {
    	this.id = id;
    	this.name = name;
    	this.phone = phone;
    	this.state = state;
    	this.country = country;
    	
    }
    
	public static Customer getRowData(String rowKey) {
		
		if(rowKey == null || rowKey.isEmpty()) {
			return null;
		}
		
		Customer c = new Customer();
		c.setId(Integer.parseInt(rowKey));

		return c;
	}

	public static String getRowKey(Customer customer) {
		return String.valueOf(customer.getId());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", phone=" + phone + ", state=" + state + ", country="
				+ country + "]";
	}
 
}