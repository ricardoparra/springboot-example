FROM openjdk:8-jdk-alpine
ARG WAR_FILE=target/*.war
COPY db/sample.db.bak db/sample.db
COPY ${WAR_FILE} app.war
ENTRYPOINT ["java","-jar","/app.war"]
