package com.project.sqlite;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

@Named
@ApplicationScoped
public class CustomerService {

	// Spring Boot will automagically wire this object using application.properties:
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public List<Customer> getAllCustomers() {
		
		String sql = "SELECT * FROM CUSTOMER ";
		
		List<Customer> customers = jdbcTemplate.query(sql, (resultSet, rowNum) -> new Customer(resultSet.getInt("id"),
				resultSet.getString("name"), resultSet.getString("phone"), resultSet.getInt("state"), resultSet.getString("country")));

		return customers;
	}

	public void updateCustomer(Customer c) {	
		jdbcTemplate.update("UPDATE CUSTOMER SET name = ?, phone = ?, state = ?, country = ? WHERE id = "+ c.getId(), 
				c.getName(), c.getPhone(), c.getState(), c.getCountry());
	}
	

	public List<Customer> getCustomersByFilter(Customer filter, int first, int pageSize, String sortField,
			String sortOrder) {

		String sql = "SELECT * FROM CUSTOMER ";

		// FILTER
		if (filter != null) {
			sql += "WHERE ";
			if (filter.getId() != null) {
				sql += "id = " + filter.getId() + " ";
			}
			if (filter.getName() != null) {
				sql += "name like '%" + filter.getName() + "%' ";
			}
			if (filter.getPhone() != null) {
				sql += "phone like '%" + filter.getPhone() + "%' ";
			}
			if (filter.getCountry() != null) {
				sql += "country like '%" + filter.getCountry() + "%' ";
			}
			if (filter.getState() != null) {
				sql += "state = " + filter.getState() + " ";
			}
		}

		// SORT
		if (sortField != null && !sortField.isEmpty()) {
			sql += "ORDER BY " + sortField + " COLLATE NOCASE ";

			if (sortOrder != null && !sortOrder.isEmpty()) {
				sql += sortOrder + " ";
			}
		}

		// LIMIT
		sql += "LIMIT " + pageSize + " OFFSET " + first;

		List<Customer> customers = jdbcTemplate.query(sql, (resultSet, rowNum) -> new Customer(resultSet.getInt("id"),
				resultSet.getString("name"), resultSet.getString("phone"), resultSet.getInt("state"), resultSet.getString("country")));

		return customers;
	}

	public long getCountCustomersByFilter(Customer filter) {

		String sql = "SELECT COUNT(*) FROM CUSTOMER ";

		// FILTER
		if (filter != null) {
			sql += "WHERE ";
			if (filter.getId() != null) {
				sql += "id = " + filter.getId() + " ";
			}
			if (filter.getName() != null) {
				sql += "name like '%" + filter.getName() + "%' ";
			}
			if (filter.getPhone() != null) {
				sql += "phone like '%" + filter.getPhone() + "%' ";
			}
			if (filter.getCountry() != null) {
				sql += "country like '%" + filter.getCountry() + "%' ";
			}
			if (filter.getState() != null) {
				sql += "state = " + filter.getState() + " ";
			}
		}

		long count = jdbcTemplate.queryForObject(sql, Long.class);

		return count;
	}

}