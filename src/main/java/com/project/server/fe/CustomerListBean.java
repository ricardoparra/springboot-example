package com.project.server.fe;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.ocpsoft.rewrite.annotation.Join;
import org.primefaces.model.LazyDataModel;

import com.project.sqlite.Customer;
import com.project.sqlite.CustomerService;

@Named("customerListBean")
@ViewScoped
@Join(path = "/", to = "/customer-list.jsf")
public class CustomerListBean {
	
	private LazyDataModel<Customer> lazyModel;

    @Inject
    private CustomerService service;

	@PostConstruct
    public void init() {
		lazyModel = new LazyCustomerDataModel(service); 
    }

    public LazyDataModel<Customer> getLazyModel() {
        return lazyModel;
    }
    
}