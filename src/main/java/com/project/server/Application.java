package com.project.server;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.webapp.FacesServlet;
import javax.inject.Inject;
import javax.servlet.DispatcherType;

import org.ocpsoft.rewrite.servlet.RewriteFilter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.project.sqlite.Customer;
import com.project.sqlite.CustomerService;

@EnableAutoConfiguration
@ComponentScan({ "com.project" })
public class Application extends SpringBootServletInitializer implements CommandLineRunner {
	
	@SuppressWarnings("serial")
	Map<String, String> regexMap = new HashMap<String, String>() {{
	    put("Cameroon", "\\(237\\)\\ ?[2368]\\d{7,8}$");
	    put("Ethiopia", "\\(251\\)\\ ?[1-59]\\d{8}$");
	    put("Morocco", "\\(212\\)\\ ?[5-9]\\d{8}$");
	    put("Mozambique", "\\(258\\)\\ ?[28]\\d{7,8}$");
	    put("Uganda", "\\(256\\)\\ ?\\d{9}$");
	}};
	
	@SuppressWarnings("serial")
	Map<String, String> countryMap = new HashMap<String, String>() {{
	    put("237", "Cameroon");
	    put("251", "Ethiopia");
	    put("212", "Morocco");
	    put("258", "Mozambique");
	    put("256", "Uganda");
	}};
	
    @Inject
    private CustomerService service;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		FacesServlet servlet = new FacesServlet();
		return new ServletRegistrationBean(servlet, "*.jsf");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean
	public FilterRegistrationBean rewriteFilter() {
		FilterRegistrationBean rwFilter = new FilterRegistrationBean(new RewriteFilter());
		rwFilter.setDispatcherTypes(
				EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST, DispatcherType.ASYNC, DispatcherType.ERROR));
		rwFilter.addUrlPatterns("/*");
		return rwFilter;
	}

	@Override
	public void run(String... args) throws Exception {			
		
		List<Customer> lst = service.getAllCustomers();
		
		for (Customer c : lst) {
						
			String idc = c.getPhone().substring(1, 4);
			
			String country = countryMap.get(idc);
			
			c.setCountry(country);
			if(c.getPhone().matches(regexMap.get(country))) {
				c.setState(1);
			}
			else {
				c.setState(0);
			}			
			
			service.updateCustomer(c);
		}

	}

}
